#include <windows.h>
#include <process.h>
#include <Tlhelp32.h>
#include <winbase.h>
#include <string.h>
void killProcessByName(const char *filename)
{
    HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
    PROCESSENTRY32 pEntry;
    pEntry.dwSize = sizeof (pEntry);
    BOOL hRes = Process32First(hSnapShot, &pEntry);
    while (hRes)
    {
        if (strcmp(pEntry.szExeFile, filename) == 0)
        {
            HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0,
                                          (DWORD) pEntry.th32ProcessID);
            if (hProcess != NULL)
            {
                TerminateProcess(hProcess, 9);
                CloseHandle(hProcess);
            }
        }
        hRes = Process32Next(hSnapShot, &pEntry);
    }
    CloseHandle(hSnapShot);
}
int main()
{
    killProcessByName("VUAgent.exe");
    killProcessByName("vim.exe");
    killProcessByName("VCSystemTray.exe");
    killProcessByName("VCService.exe");
    killProcessByName("VCAgent.exe");
    killProcessByName("VAIOUpdt.exe");
    killProcessByName("TeamViewer_Service.exe");
    killProcessByName("SkypeC2CPNRSvc.exe");
    killProcessByName("SkypeC2CAutoUpdateSvc.exe");
    killProcessByName(".exe");
    killProcessByName("HDSentinel.exe");
    killProcessByName("GWX.exe");
    killProcessByName("BTTray.exe");
    killProcessByName("armsvc.exe");
    return 0;
}
